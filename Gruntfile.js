module.exports = function (grunt)
{
    require('time-grunt')(grunt);
    require('jit-grunt')(grunt, {
        useminPrepare: 'grunt-usemin'
    });

    grunt.initConfig({  /// revisado   grunt.initConfig
        sass: {
            dist: {
                files: [{
                    expand: true,
                    cwd: 'css',
                    src: ['*.scss'],
                    dest: 'css',
                    ext: '.css'
                }]
            }
        },

        watch: {
            files: ['css/*.scss'],
            tasks: ['css']
        },

        browserSync: {
            dev: {
                bsFiles: { //browser files
                    src: [
                        'css/*.css', // estos son los archivos que tiene que mirar
                        '*.html',
                        'js/*.js'
                    ]
                },

                options: {
                    watchTask: true,
                    server: {
                        baseDir: './' //directorio base de nuestro sevidor
                    }
                }
            }, // lave dev
            
        }, // lave browserSync

        imagemin: { /// revisado 
                dynamic: {
                    files: [{
                        expand: true,
                        cwd: './',
                        src: 'images/*.{png,gif,jpg,jpeg}',
                        dest: 'dist/'
                    }]
                }
        },
        
        copy: {  /// revisado 
            html: {
                files: [{
                    expand: true,
                    dot: true,
                    cwd: './',
                    src: ['*.html'],
                    dest: 'dist'
                }]
            },
            fonts: {

                files: [
                {
                    // for fonts-awesome
                        expand: true,
                        dot: true,
                        cwd: 'node-modules/open-iconic/fonts',
                        src: ['fonts/*.*'],
                        dest: 'dist'
                }]
            }
        },

        clean: { /// revisado 
            build: {
                src: ['dist/']
            }
        },
        cssmin: { /// revisado 
            dist: {}
        },

        uglify: { /// revisado 
            dist: {}
        },
        /// filerev genera un codigo que no sea cacheable 
        filerev: { /// revisado 
            options: {
                encoding: 'utf8',
                algorithm: 'md5',
                length: 20
            },

            release: {
                // filesrev:release hashes(md5) all assets (images, js and css)
                // in dist directorio
                files: [{
                    src: [
                        'dist/js/*.js',
                        'dist/css/*.css', // no deveria estar con coma
                    ]
                }]
            }
        }, // fin filerev 

        concat: {  /// revisado 
            options: {
                separator: ';'
            },
            dist: {}
        },

        useminPrepare: { // revisado
            foo: {
                dest: 'dist',
                src: ['index.html', 'about.html', 'precios.html', 'contacto.html', 'terminos.html']
            },
            options: {
                flow: {
                    steps: {
                        css: ['cssmin'],
                        js: ['uglify']
                    },
                    post: {
                        css: [{
                            name: 'cssmin',
                            createConfig: function(context, block)
                            {
                                var generated = context.options.generated;
                                generated.options = {
                                    keepSpecialComments: 0,
                                    rebase: false
                                }
                            }
                        }]
                    }
                }
            } // fin options
        }, // fin eminPrepare

        usemin: {
            html: ['dist/index.html', 'dist/about.html', 'dist/precios.html', 'dist/terminos.html'],
            options: {
                assetsDir: ['dist', 'dist/css', 'dist/js']
            }
        } // fin usemin
    });
   /* grunt.loadNpmTasks('grunt-contrib-watch'); // agregando las tareas
    grunt.loadNpmTasks('grunt-contrib-sass');
    grunt.loadNpmTasks('grunt-browser-sync');
    grunt.loadNpmTasks('grunt-contrib-imagemin');*/

    grunt.registerTask('css', ['sass']);
    grunt.registerTask('default', ['browserSync', 'watch']);
    grunt.registerTask('img:compress', ['imagemin']);
    grunt.registerTask('build', [
        'clean',
        'copy',
        'imagemin',
        'useminPrepare',
        'concat',
        'cssmin',
        'uglify',
        'filerev',
        'usemin'

    ])

};