$(function ()
{
    $('[data-toggle="tooltip"]').tooltip();
    $('[data-toggle="popover"]').popover();
    $('.carousel').carousel({
        inteervel: 3000
    });
    $('#contacto').on('show.bs.modal', function (e)
    {
        console.log('El modal se esta mostrando');

        $('#contactoBtn').removeClass('btn-secondary');
        $('#contactoBtn').addClass('btn-default');
        $('contactoBtn').prop('disabled', true);
    });
    $('#contacto').on('shown.bs.modal', function (e)
    {
        console.log('El modal se mostro');
    });
    $('#contacto').on('hidde.bs.modal', function (e)
    {
        console.log('El modal se esta ocultando');
    });
    $('#contacto').on('hidden.bs.modal', function (e)
    {
        console.log('El modal se oculto');
        $('contactoBtn').prop('disabled', false);
    });
});